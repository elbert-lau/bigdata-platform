systemctl start docker
docker run --rm \
--name bigdata01.docker.com \
--hostname bigdata01.docker.com \
--privileged=true \
-p 8088:8088 \
-p 50070:50070 \
-p 18080:18080 \
-p 19888:19888 \
-p 16010:16010 \
-p 50090:50090 \
-p 8020:8020 \
-p 8998:8998 \
-v /root/workspace/bigdata-platform/apache0.10.0/data:/opt/data:rw \
-v /root/workspace/bigdata-platform/apache0.10.0/logs:/opt/logs:rw \
-v /root/workspace/bigdata-platform/apache0.10.0/conf:/opt/conf:rw \
-v /root/workspace/bigdata-platform/apache0.10.0/jars:/opt/jars:rw \
-it elbertmalone/bigdata-platform:apache0.10.0
